sudo apt install libassimp-dev freeglut3 libglu1-mesa-dev freeglut3-dev mesa-common-dev
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=~/workspace/oss -DBUILD_sdurw_java=FALSE -DBUILD_sdurw_lua=FALSE ..
make install -j4